<?php
use Silex\Application;
use Symfony\Component\Translation\Loader\JsonFileLoader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//DOCTRINE
require_once __DIR__ . '/../config/prod.php';

define("ROOT", __DIR__ . "/../");

//REGISTER CONTROLLER AS SERVICE
$app->register(new Silex\Provider\ServiceControllerServiceProvider());


//TEMPLATE ENGINE
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => array(__DIR__.'/../src')
));

//LOG
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/development.log',
));

//DOCTRINE
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array (
        'driver' => 'pdo_mysql',
        'host' => getenv('HOST'),
        'port' => getenv('PORT'),
        'user' => getenv('USER'),
        'password' => getenv('PASSWORD'),
        'dbname' => getenv('DATABASE'),
        'charset'   => 'utf8'
    ),
));

//$a = $app['db']->fetchAll('SELECT * FROM usuario');

//INTERNACIONALIZAÇÃO
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('pt-br'),
));

$lang = "pt-br";

$app['locale'] = $lang;
$app['translator']->addLoader('json', new JsonFileLoader());


$finder = new Finder();
$finder->files()->in(__DIR__.'/../lang');

foreach ($finder as $file) {

    $sLangFile = strstr($file->getFilename(), '.', true);
    $sPathFile = $file->getRealpath();

    $app['translator']->addResource('json', $sPathFile, $sLangFile);
}

if(!$app['debug']){
    $app->error(function (\Exception $e, $code) use ($app) {
        switch ($code) {
            case 404:
                $message = '404 - The requested page could not be found.';

                return $app['twig']->render('Site/Templates/404.twig', array(
                    'name' => 'Christian'
                ));
                break;
            case 500:
                $message = 'Erro 500';
                break;
            default:
                $message = 'We are sorry, but something went terribly wrong.';
        }

        return new Response($message);
    });
}


$app['timer'] = 0;
$app['memory'] = 0;

$app->before(function (Request $request, Application $app) {
    $app['request'] = $request;

    if ($request->getMethod() === 'OPTIONS') {
        return;
    }

    $app['timer'] = microtime(true);
    $app['memory'] = memory_get_usage(true);
}, Application::EARLY_EVENT);

$app->after(function (Request $request, Response $response, $app) {
    if ($request->getMethod() === 'OPTIONS') {
        return $response;
    }

    $response->headers->set('time', round((microtime(true) - $app['timer']), 4) . ' s');
    $response->headers->set('memory', (memory_get_usage(true) - $app['memory'])/1024 . ' KB');
//    $response->send();

    return $response;
}, Application::LATE_EVENT);

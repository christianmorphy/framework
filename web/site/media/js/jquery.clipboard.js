/* 
* @author Niklas von Hertzen <niklas at hertzen.com>
* @created 14.6.2011 
* @website http://hertzen.com
 */


(function( $ ){
    $.fn.clipboard = function(options) {     
        var defaults = {
            prepend: null, // content to prepend to copy selection
            append: null,  // content to append to copy selection
            disable:false,  // disable copying for element
            oncopy: function(content){} // callback on copy event
                            
        };
                    
        var options = $.extend({},defaults,options);
        
        $(this).each(function(i,el){

            el.oncopy = function(e,b){
                if (options.disable){
                    return false;
                }
                if (window.clipboardData && document.selection) { // Internet Explorer
                            
                            
         
                    var s = document.selection;

                    var r = s.createRange();
                    var t = r.htmlText;
                    
                    if (options.prepend!==null){
                        t = options.prepend + t;
                    }
                            
                    if (options.append!==null){
                        t = t + options.append;
                    }
                                
                    options.oncopy(t);
                    
                    if (window.clipboardData.setData ("Text", t)){
                        return false;
                    }
                    
                }else {
                    // the rest (which don't support clipboardData)
                    
                    var r = document.createRange();
                    
                    var obj = $('body').append('<div id="teste" style="width: 0px; height: 0px; overflow: hidden; zoom: 1;">teste</div>');
                    
                    var w=document.getElementById("teste");  
                    
                    $(obj).remove();
                
                    r.selectNodeContents(w);  
                    var sel=window.getSelection(); 
                    sel.removeAllRanges(); 
                    sel.addRange(r);
                    
                    options.oncopy(sel.toString());
                    
                    
                }
            };
        });
                    
                    
        return this;
    }
})( jQuery );

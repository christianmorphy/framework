function retornaExtensao(Caminho){
    Caminho     = Caminho.replace('/\/g', "/");
    var Arquivo = Caminho.substring(Caminho.lastIndexOf('/') + 1);
    var Extensao= Arquivo.substring(Arquivo.lastIndexOf('.') + 1);
    return {arquivo:Arquivo, extensao:Extensao};
}

/**
 * 
 * @param {String} sCaminho
 * @param {String} sExtensoes Ex.: doc,docx,txt,odt;
 */
function validaExtensao(oObj, sExtensoes){
	
	var oBjExt = retornaExtensao(oObj.val());
	
	if( sExtensoes.indexOf( oBjExt.extensao ) == -1 ){ 
	    alert( oBjExt.arquivo + "\n \n Não possui é extensão válida, favor selecionar apenas "+sExtensoes );
	    
	    return false;
	}
}

String.prototype.replaceAll = function(str1, str2, ignore){
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};
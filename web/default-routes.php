<?php
use Silex\Application;
use API\Abstracts\AbstractController;

use Common\Exception\NotFoundException;

$app->get('/teste-parse', function (Application $app) {
   echo '<pre>';
    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository('API\\Entity\\User');
    $abstractController = new AbstractController($app, $repo);

   $aSearch = $abstractController->parseSearchParams2();

   print_r($aSearch);
   exit;
});
$app->get('/{controller}/{action}/{id}', function ($controller, $action, $id, Application $app) {
    $appDefault = $app['request']->server->get('APP_DEFAULT');

    if (isset($appDefault)) {
        $application = $appDefault;
    }
//    $appDefault = $app['request']->server->get('APP_DEFAULT');

    $application = ucfirst($application);
    $controller = ucfirst($controller);

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->value('action', 'index')
->value('id', '')
->assert('id', '\d+')
->bind('default-route-get');

//Rota Padrão
$app->get('/{application}/{controller}/{action}/{id}', function ($application, $controller, $action, $id, Application $app) {
    $application = ucfirst($application);
    $controller = ucfirst($controller);

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->value('action', 'index')
->value('id', '')
->bind('default-route-get2');


$app->post('/{application}/{controller}/{action}', function ($application, $controller, $action, Application $app) {
    $application = ucfirst($application);
    $controller = ucfirst($controller);

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->value('action', 'post')
->bind('default-route-post');

$app->post('/{controller}/{action}', function ($controller, $action, Application $app) {
    $appDefault = $app['request']->server->get('APP_DEFAULT');

    if (isset($appDefault)) {
        $application = $appDefault;
    }

    $application = ucfirst($application);
    $controller = ucfirst($controller);

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->bind('default-route-post2')
->value('action', 'post');


$app->put('/{application}/{controller}', function ($application, $controller, Application $app) {
    $application = ucfirst($application);
    $controller = ucfirst($controller);
    $action = 'update';

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->bind('default-route-put');

$app->delete('/{application}/{controller}/{id}', function ($application, $controller, $id, Application $app) {
    $application = ucfirst($application);
    $controller = ucfirst($controller);
    $action = 'delete';

    $sClass = $application.'\\Modules\\'.$controller.'\\'.$controller.'Controller';

    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository($application.'\\Entity\\'.$controller);

    $abstractController = new AbstractController($app, $repo);

    if (class_exists($sClass)){
        $oTemp = new $sClass($app, $repo);

        if (method_exists($oTemp, $action)) {
            return $oTemp->$action($app);
        }
        else {
            throw new NotFoundException();
        }
    }
    else if (method_exists($abstractController, $action)) {
        return $abstractController->$action($app);
    }
    else{
        return $app['twig']->render('Admin/Templates/404.twig', array(
            'name' => 'Christian'
        ));
    }
})
->value('id', '')
->bind('default-route-delete');

$app->match('{route}', function () {
    return new \Symfony\Component\HttpFoundation\Response(
        '',
        200,
        [
            'Allow' => 'OPTIONS, POST, GET, PUT, DELETE, PATCH',
            'Access-Control-Allow-Methods' => 'OPTIONS, POST, GET, PUT, DELETE, PATCH'
        ]
    );
})->assert('route', '.+')
    ->method('OPTIONS');

$app->error(function (\Exception $e, \Symfony\Component\HttpFoundation\Request $request, $code) use ($app) {
    $aReturn = array("message" => $e->getMessage(), "code" => $code, "data" => array());
    return new \Symfony\Component\HttpFoundation\JsonResponse($aReturn, $code, ['Content-Type' => 'application/json']);
});
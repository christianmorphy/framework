<?php

$headers = [
    'Authorization',
    'Client',
    'Content-type',
    'Platform',
    'Access-Control-Allow-Origin',
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Methods'
];

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: ' . implode(', ', $headers));

date_default_timezone_set('America/Sao_Paulo');

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

date_default_timezone_set('America/Sao_Paulo');
const REQUEST_VALIDATE = 512;

$loader = require_once __DIR__.'/../vendor/autoload.php';

require_once '../env.php';

if (getenv('DEBUG')) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
}

$app = new Application();
$app['debug'] = getenv('DEBUG');

require_once('bootstrap.php');


$app->get('/', function () {
    $teste = '';
    for ($i=0; $i<=1000000; $i++){
        $teste .= 'teste';
    }

    return new JsonResponse(["code" => 200, "message" => "Registros encontrados.", "data" => "teste"]);
});
$app->get('/Admin/Contato', 'admin.contato:testeFunction')->bind('SiteContato');

$app->get('/Contato', 'site.contato:index');
$app->get('/contacts.json', 'api.contato:get');

$app->get('/API/Products/getAll', function (Application $app) {

    $client = new SoapClient( getenv('HOST_MAGENTO').'/api/soap/?wsdl');
//    $client = new SoapClient( getenv('HOST_MAGENTO').'/api/v2_soap?wsdl=1');

    // If somestuff requires api authentification,
    // then get a session token
    $session = $client->login('asp', 'goleiro2012');

    $filters = array(array('type' => array('like'=>'configurable')));
    $params = array('filter' => array(
        array('key' => 'type', 'value' => 'configurable'),
    ));
    $result = $client->call($session, 'catalog_product.list', $filters);
//    $result = $client->catalogProductList($session, $params);

    foreach ($result as $key => $item) {
//        $resultImages = $client->call($session, 'catalog_product.info', $item['sku'].' ');
        $resultImages2 = $client->call($session, 'catalog_product_attribute_media.list', $item['sku'].' ');
//        $resultImages = $client->catalogProductInfo($session, $item->sku.' ');
//        $result[$key]['info'] = $resultImages;
        $result[$key]['images'] = $resultImages2;
    }

// If you don't need the session anymore
    $client->endSession($session);

    return new JsonResponse(["code" => 200, "message" => "Registros encontrados.", "data" => $result]);
});

$app->get('/API/autorizeOld', function (Application $app) {
    /**
     * Example of products list retrieve using Customer account via Magento REST API. OAuth authorization is used
     */

    $callbackUrl = getenv('HOST_API')."/API/autorize";
    $temporaryCredentialsRequestUrl = getenv('HOST_MAGENTO')."/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
    $adminAuthorizationUrl = getenv('HOST_MAGENTO').'/admin/oauth_authorize';
    $accessTokenRequestUrl = getenv('HOST_MAGENTO').'/oauth/token';
    $apiUrl = getenv('HOST_MAGENTO').'/api/rest';

    $consumerKey = getenv('CONSUMER_KEY_MAGENTO');
    $consumerSecret = getenv('SECRET_KEY_MAGENTO');

    $secret = getenv('SECRET');
    $token = getenv('TOKEN');

    session_start();

//    echo '<pre>';
//    var_dump($callbackUrl);
//    var_dump($temporaryCredentialsRequestUrl);
//    var_dump($adminAuthorizationUrl);
//    var_dump($accessTokenRequestUrl);
//    var_dump($apiUrl);
//    exit;

    if (!isset($_GET['oauth_token']) && isset($_SESSION['state']) && $_SESSION['state'] == 1) {
        $_SESSION['state'] = 0;
    }
    try {
        $authType = ($_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
        $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
        $oauthClient->enableDebug();

        if (!isset($_GET['oauth_token']) && !$_SESSION['state']) {
            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);

            $_SESSION['secret'] = $requestToken['oauth_token_secret'];
            $_SESSION['state'] = 1;
            header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
            exit;
        } else if ($_SESSION['state'] == 1) {
            $oauthClient->setToken($_GET['oauth_token'], $_SESSION['secret']);
            $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
            $_SESSION['state'] = 2;
            $_SESSION['token'] = $accessToken['oauth_token'];
            $_SESSION['secret'] = $accessToken['oauth_token_secret'];

            echo '<pre>';
            print_r($_SESSION);
            var_dump($secret);
            var_dump($token);
            exit;

            header('Location: ' . $callbackUrl);
            exit;
        }
    } catch (OAuthException $e) {
        print_r($e);
    }
    exit;
});

$app->get('/API/Products/getAllOld', function (Application $app) {

    /**
     * Example of products list retrieve using Customer account via Magento REST API. OAuth authorization is used
     */
    $callbackUrl = getenv('HOST_API')."/API/Products/getAll";
    $temporaryCredentialsRequestUrl = getenv('HOST_MAGENTO')."/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
    $adminAuthorizationUrl = getenv('HOST_MAGENTO').'/admin/oauth_authorize';
    $accessTokenRequestUrl = getenv('HOST_MAGENTO').'/oauth/token';
    $apiUrl = getenv('HOST_MAGENTO').'/api/rest';

    $consumerKey = getenv('CONSUMER_KEY_MAGENTO');
    $consumerSecret = getenv('SECRET_KEY_MAGENTO');

    $secret = getenv('SECRET');
    $token = getenv('TOKEN');

    session_start();

    try {
//        $authType = ($_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
        $authType = OAUTH_AUTH_TYPE_URI;
        $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
//        $oauthClient->enableDebug();

        $oauthClient->setToken(getenv('TOKEN'), getenv('SECRET'));
        $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, ['username' => 'tiago', 'password' => 'goleiro2012']);

        echo 'entrou'; exit;

        $token = $accessToken['oauth_token'];
        $secret = $accessToken['oauth_token_secret'];

        $oauthClient->setToken($token, $secret);
        $resourceUrl = "$apiUrl/products";
        $oauthClient->fetch($resourceUrl);

        $productsList = json_decode($oauthClient->getLastResponse());

//        foreach ($productsList as $product){
//            if ($product->type_id == 'simple') {
//                $resourceUrl = "$apiUrl/products/".$product->sku.'/images';
//                $oauthClient->fetch($resourceUrl);
//                $images = json_decode($oauthClient->getLastResponse());
//
//                $product->images = $images;
//            }
//        }

        return new JsonResponse(["code" => 200, "message" => "Registros encontrados.", "data" => $productsList]);
    } catch (OAuthException $e) {
        return new JsonResponse(["code" => 200, "message" => "Registros encontrados.", "data" => $e]);
    }
});

$app->get('/magento', function (Application $app) {

//    phpinfo(); exit;

    /**
     * Example of retrieving the products list using Admin account via Magento REST API. OAuth authorization is used
     * Preconditions:
     * 1. Install php oauth extension
     * 2. If you were authorized as a Customer before this step, clear browser cookies for getenv('HOST_MAGENTO').'/'
     * 3. Create at least one product in Magento
     * 4. Configure resource permissions for Admin REST user for retrieving all product data for Admin
     * 5. Create a Consumer
     */
    // $callbackUrl is a path to your file with OAuth authentication example for the Admin user

    $callbackUrl = getenv('HOST_API')."/API/Products/getAll";
    $temporaryCredentialsRequestUrl = getenv('HOST_MAGENTO')."/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
    $adminAuthorizationUrl = getenv('HOST_MAGENTO').'/admin/oauth_authorize';
    $accessTokenRequestUrl = getenv('HOST_MAGENTO').'/oauth/token';
    $apiUrl = getenv('HOST_MAGENTO').'/api/rest';

    $consumerKey = getenv('CONSUMER_KEY_MAGENTO');
    $consumerSecret = getenv('SECRET_KEY_MAGENTO');

    session_start();

//    session_destroy();
//    exit;

    if (!isset($_GET['oauth_token']) && isset($_SESSION['state']) && $_SESSION['state'] == 1) {
        $_SESSION['state'] = 0;
    }
    try {
        $authType = ($_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;

        $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
        $oauthClient->enableDebug();

        if (!isset($_GET['oauth_token']) && !$_SESSION['state']) {
            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);

            $_SESSION['secret'] = $requestToken['oauth_token_secret'];
            $_SESSION['state'] = 1;

            header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
            exit;
        } else if ($_SESSION['state'] == 1) {
            $oauthClient->setToken($_GET['oauth_token'], $_SESSION['secret']);
            $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
            $_SESSION['state'] = 2;
            $_SESSION['token'] = $accessToken['oauth_token'];
            $_SESSION['secret'] = $accessToken['oauth_token_secret'];

            header('Location: ' . $callbackUrl);
            exit;
        } else {
            $oauthClient->setToken($_SESSION['token'], $_SESSION['secret']);

            $resourceUrl = "$apiUrl/products";
//            $resourceUrl = "$apiUrl/stockitems/1";
            $data = array("qty" => "8");

            $oauthClient->disableRedirects();
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
//            $oauthClient->fetch($resourceUrl, json_encode($data), 'PUT', $headers);
            $oauthClient->fetch($resourceUrl, array(), 'GET', $headers);

            $productsList = json_decode($oauthClient->getLastResponse());

            echo '<pre>';
            print_r($productsList);
        }
    } catch (OAuthException $e) {
        print_r($e->getMessage());
//        echo "&lt;br/&gt;";
        print_r($e->lastResponse);
    }

    exit;
});

include (__DIR__ . '/../src/API/Modules/Auth/Resources/routes.php');
include (__DIR__ . '/default-routes.php');

$app->run();

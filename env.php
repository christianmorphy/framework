<?php

if (!file_exists(__DIR__.'/set-env.php')) {
    exit('Você precisa configurar as variáveis de ambiente em seu arquivo set-env.php (ver exemplo em set-env.php.example)');
}

include_once __DIR__.'/set-env.php';

$ENV = [];

$ENV['LOCAL'] = [];
$ENV['DEVELOP'] = [];
$ENV['HOMOLOG'] = [];
$ENV['PRODUCTION'] = [];

$ENV['LOCAL']['HOST'] = 'localhost';
$ENV['LOCAL']['PORT'] = '3306';
$ENV['LOCAL']['USER'] = 'root';
$ENV['LOCAL']['PASSWORD'] = '';
$ENV['LOCAL']['DATABASE'] = 'framework';
$ENV['LOCAL']['HOST_API'] = 'http://framework.local.com';
$ENV['LOCAL']['HOST_MAGENTO'] = 'http://loja.local.com';
$ENV['LOCAL']['CONSUMER_KEY_MAGENTO'] = '52a424d58c7aed73c17e1f2b253fca6e';
$ENV['LOCAL']['SECRET_KEY_MAGENTO'] = '1c4acc912afca535c003f2dcf9db9eea';
$ENV['LOCAL']['DEBUG'] = true;

$ENV['DEVELOP']['HOST'] = 'localhost';
$ENV['DEVELOP']['PORT'] = '3306';
$ENV['DEVELOP']['USER'] = 'root';
$ENV['DEVELOP']['PASSWORD'] = 'root';
$ENV['DEVELOP']['DATABASE'] = 'framework';
$ENV['DEVELOP']['HOST_API'] = 'http://framework.local.com';
$ENV['DEVELOP']['HOST_MAGENTO'] = 'http://www.proluva.com.br';
$ENV['DEVELOP']['CONSUMER_KEY_MAGENTO'] = '8e7a833454beabb0c2902edb812538df';
$ENV['DEVELOP']['SECRET_KEY_MAGENTO'] = 'd3e2ffea451b94594e6b8082230022c6';
$ENV['DEVELOP']['SECRET'] = 'b761eb29874a10cd3f2325de86dcac0f';
$ENV['DEVELOP']['TOKEN'] = '72256d7bc8a8b9a1cda71b16ef05178d';
$ENV['DEVELOP']['DEBUG'] = true;

$ENV['HOMOLOG']['HOST'] = 'localhost';
$ENV['HOMOLOG']['PORT'] = '3306';
$ENV['HOMOLOG']['USER'] = 'root';
$ENV['HOMOLOG']['PASSWORD'] = '';
$ENV['HOMOLOG']['DATABASE'] = 'framework';
$ENV['HOMOLOG']['HOST_API'] = 'http://framework.local.com';
$ENV['HOMOLOG']['HOST_MAGENTO'] = 'http://www.proluva.com.br';
$ENV['HOMOLOG']['CONSUMER_KEY_MAGENTO'] = '7e0c80ce942c42ac58bf2835d641d145';
$ENV['HOMOLOG']['SECRET_KEY_MAGENTO'] = '4ab6dede68e028bd32b2110d5ed47fb2';
$ENV['HOMOLOG']['SECRET'] = 'cda68042aaaffb7d69ed1ec2e6fb53c2';
$ENV['HOMOLOG']['TOKEN'] = '70a9118ed19398c1ae01e2d4ef42ce43';
$ENV['HOMOLOG']['DEBUG'] = true;

$ENV['PRODUCTION']['HOST'] = 'localhost';
$ENV['PRODUCTION']['PORT'] = '3306';
$ENV['PRODUCTION']['USER'] = 'root';
$ENV['PRODUCTION']['PASSWORD'] = '1sKahKUysJXSjlekFHLX';
$ENV['PRODUCTION']['DATABASE'] = 'asp';
$ENV['PRODUCTION']['HOST_API'] = 'http://asp01.hosoccer.com.br/api/index.php';
$ENV['PRODUCTION']['HOST_MAGENTO'] = 'http://www.proluva.com.br';
$ENV['PRODUCTION']['CONSUMER_KEY_MAGENTO'] = '8e7a833454beabb0c2902edb812538df';
$ENV['PRODUCTION']['SECRET_KEY_MAGENTO'] = 'd3e2ffea451b94594e6b8082230022c6';
$ENV['PRODUCTION']['SECRET'] = 'a84575287474bc0251664bc324416e81';
$ENV['PRODUCTION']['TOKEN'] = '51931a3e13b7843af9bc1dffa81f3cbe';
$ENV['PRODUCTION']['DEBUG'] = true;

if (defined('ENVIRONMENT') && count($ENV[ENVIRONMENT]) > 0) {

    putenv("ENVIRONMENT=".ENVIRONMENT);
    
    foreach ($ENV[ENVIRONMENT] as $key => $env){
        putenv($key."=$env");
    }
}
else {
    exit('Configurar o enviroment.');
}







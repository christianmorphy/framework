<?php

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="framework.local.com/API",
 *     basePath="/",
 *     @SWG\Info(
 *         version="0.0.1",
 *         title="Proluva ASP API",
 *         description="Proluva ASP API",
 *         termsOfService=""
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     @SWG\Tag(name="tags", description="teste", x="array"),
 *     path="/User/getAll",
 *     @SWG\Response(response="200", description="An example resource")
 * )
 */

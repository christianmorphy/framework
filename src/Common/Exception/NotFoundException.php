<?php
namespace Common\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class NotFoundException
 * @package Common\Exception
 */
class NotFoundException extends HttpException
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Essa rota não existe.', $code = Response::HTTP_NOT_FOUND, \Exception $previous = null)
    {
        parent::__construct($code, $message, $previous);
    }
}

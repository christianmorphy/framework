<?php
namespace Common\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class InvalidArgumentHttpException
 * @package Common\Exception
 */
class InvalidArgumentHttpException extends HttpException
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = null, $code = Response::HTTP_OK, \Exception $previous = null)
    {
        parent::__construct($code, $message, $previous);
    }
}

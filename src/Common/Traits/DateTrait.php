<?php
namespace Common\Traits;

/**
 * Class DateTrait
 * @package Common\Traits
 */
trait DateTrait
{
    /**
     * @var \DateTime|null
     */
    protected $created_at;

    /**
     * @var \DateTime|null
     */
    protected $updated_at;

    /**
     * @var \DateTime|null
     */
    protected $deleted_at;

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime|null $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTime|null $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @param \DateTime|null $deleted_at
     */
    public function setDeletedAt($deleted_at)
    {
        $this->deleted_at = $deleted_at;
    }


}
<?php
namespace Common\Traits;

/**
 * Class ReferenceCodeTrait
 * @package Common\Traits
 */
trait ReferenceCodeTrait
{
    protected $reference_code;

    /**
     * @return mixed
     */
    public function getReferenceCode()
    {
        return $this->reference_code;
    }

    /**
     * @param mixed $reference_code
     */
    public function setReferenceCode($reference_code)
    {
        $this->reference_code = $reference_code;
    }
}
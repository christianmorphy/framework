<?php
namespace Common\Util;

use Symfony\Component\HttpFoundation\Request;

class TransformRequest
{

    /**
     * @param Request $request
     * @return mixed
     */
    public static function transformContentToArray ($request)
    {
        return json_decode($request->getContent(), true);
    }

    public static function transformQueryToArray ($request)
    {
        return $request->query->all();
    }

    public static function getAttribute ($request, $param)
    {
        return $request->attributes->get($param);
    }
}
<?php
namespace Admin\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Contato
{

    public function index(Application $app)
    {
        $app['locale'] = 'pt-br';
        return $app['twig']->render('Admin/Views/teste-form3.twig', array(
            'name' => 'Christian',
        ));
    }


    public function testeFunction(Application $app)
    {
        $app['locale'] = 'pt-br';

        return $app['twig']->render('Admin/Views/teste-form.twig', array(
            'name' => 'Christian',
        ));
    }

    public function testeFunction2(Application $app)
    {
        return $app['twig']->render('Admin/Views/teste-form.twig', array(
            'name' => 'Christian',
        ));
    }
}

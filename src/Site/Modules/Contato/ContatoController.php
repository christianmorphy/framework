<?php

namespace Site\Modules\Contato;

use Silex\Application;

/**
 * Class ContatoController
 * @package Site\Modules\Contato
 */
class ContatoController
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app)
    {
        /** @var \Twig_Environment $twig */
        $twig = $app['twig'];

        return $twig->render('Site/Modules/Contato/Views/index.twig');
    }
}

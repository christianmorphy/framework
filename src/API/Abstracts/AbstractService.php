<?php
namespace API\Abstracts;

use Common\Exception\InvalidArgumentHttpException;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Common\Util\TransformRequest;

abstract class AbstractService
{
    private $em;
    private $metadata;
    private $db;

    /**
     * AbstractService constructor.
     * @param $app Application
     * @param $em EntityManager
     */
    public function __construct($app, $repo)
    {
        $this->em = $app['Doctrine\ORM\EntityManager'];
        $this->repo = $repo;
        $this->db = $app['db'];
        $this->metadata = $this->em->getClassMetadata($repo->getClassName());
    }
    public function parseFieldsParmas() {
        $sFieldsReturn = '';

        $aFields = array_keys($this->metadata->fieldMappings);

        $sFieldsReturn = implode(',', $aFields);

        return $sFieldsReturn;
    }

    public function parseSearchArrayParams ($aSearch) {
        $aSearchFields = array();

        if (count($aSearch) > 0) {
            $aSearchFields[0] = array('op' => '=', 'expr' => 'and');

            foreach ($aSearch as $key => $item) {
                $aSearchFields[0]['values'][] = array('field' => $key, 'value' => $item);
            }
        }

        return $aSearchFields;
    }

    public function parseSearchParams ($sSearch) {
        $aSearchFields = array();

        if (isset($sSearch)) {
            $aDataSearch = explode(',', $sSearch);

            if (count($aDataSearch) > 0) {
                foreach ($aDataSearch as $key => $fieldSearch) {
                    $aExplodeFieldsSearch = explode('|', $fieldSearch);
                    if (count($aExplodeFieldsSearch) > 0) {
                        $aSearchFields[$key] = array();
                        foreach ($aExplodeFieldsSearch as $aField) {
                            $aExplodeFieldSearch = explode('=', $aField);

                            if ($aExplodeFieldSearch[0] == 'expr') {
                                $aSearchFields[$key][$aExplodeFieldSearch[0]] = $aExplodeFieldSearch[1];
                            }
                            else if ($aExplodeFieldSearch[0] == 'op') {
                                $sOperation = '';
                                switch ($aExplodeFieldSearch[1]) {
                                    //operators: like (like), = (equal), <> (diff), > (moreThan), >= (moreEqualThan), < (lessThan), <= (lessThan)

                                    case 'equal':
                                        $sOperation = '=';
                                        break;
                                    case 'like':
                                        $sOperation = 'like';
                                        break;
                                    case 'diff':
                                        $sOperation = '<>';
                                        break;
                                    case 'moreThan':
                                        $sOperation = '>';
                                        break;
                                    case 'moreEqualThan':
                                        $sOperation = '>=';
                                        break;
                                    case 'lessThan':
                                        $sOperation = '<';
                                        break;
                                    case 'lessEqualThan':
                                        $sOperation = '<=';
                                        break;
                                }

                                $aSearchFields[$key][$aExplodeFieldSearch[0]] = $sOperation;
                            }
                            else {
                                $aSearchFields[$key]['values'][] = array('field' => $aExplodeFieldSearch[0], 'value' => $aExplodeFieldSearch[1]);
                            }
                        }

                        if (!isset($aSearchFields[$key]['expr'])) {
                            $aSearchFields[$key]['expr'] = 'and';
                        }
                        if (!isset($aSearchFields[$key]['op'])) {
                            $aSearchFields[$key]['op'] = '=';
                        }
                    }
                }
            }
        }

        return $aSearchFields;
    }

    public function parseSearchParamsOld ($sSearch) {
        $aSearchFields = array();

        if (isset($sSearch)) {
            $aDataSearch = explode(',', $sSearch);

            if (count($aDataSearch) > 0) {
                foreach ($aDataSearch as $fieldSearch) {
                    $aExplodeFieldSearch = explode('=', $fieldSearch);
                    $aSearchFields[$aExplodeFieldSearch[0]] = $aExplodeFieldSearch[1];
                }
            }
        }

        return $aSearchFields;
    }
    public function parseSortParams ($sSort) {
        $aSearchSorts = array();

        if (isset($sSort)) {
            $aDataSort = explode(',', $sSort);

            if (count($aDataSort) > 0) {
                foreach ($aDataSort as $fieldSort) {
                    $aExplodeFieldSort = explode(':', $fieldSort);
                    $aSearchSorts[$aExplodeFieldSort[0]] = $aExplodeFieldSort[1];
                }
            }
        }

        return $aSearchSorts;
    }

    public function customQuery ($data, $app) {
        $aReturn = array();

        $aSearchFields = array();
        $aSearchSorts = array();

        if (isset($data['search'])) {
            $aSearchFields = $this->parseSearchParams($data['search']);
        }
        else if (isset($data['searchArray'])) {
            $aSearchFields = $this->parseSearchArrayParams($data['searchArray']);
        }

        if (isset($data['sort'])) {
            $aSearchSorts = $this->parseSortParams($data['sort']);
        }

        if (!isset($data['fields'])) {
            $data['fields'] = $this->parseFieldsParmas();
        }

        if (isset($data['objects'])) {
            $data['objects'] = explode(',', $data['objects']);
        }
        else {
            $data['objects'] = array();
        }

        /** @var QueryBuilder $qb */
        $qb = $this->db->createQueryBuilder();

        $qb->select($data['fields'])
            ->from($this->metadata->table['name'])
            ->where('deleted_at IS NULL');



        if (count($aSearchFields) > 0) {
            $qb->andWhere('1 = 1');

            foreach ($aSearchFields as $key => $aSearchField) {
                foreach ($aSearchField['values'] as $key2 => $aValue) {

                    if ($aSearchField['expr'] == 'and' || $key == 0) {
                        $qb->andWhere($aValue['field'].' '.$aSearchField['op'].' :'.'field'.$key.$key2)
                            ->setParameter(':'.'field'.$key.$key2, $aValue['value']);
                    }
                    else if ($aSearchField['expr'] == 'or') {
                        $qb->orWhere($aValue['field'].' '.$aSearchField['op'].' :'.'field'.$key.$key2)
                            ->setParameter(':'.'field'.$key.$key2, $aValue['value']);
                    }
                }
            }
        }

        if (count($aSearchSorts) > 0) {
            foreach($aSearchSorts as $key => $aSearchSort) {
                $qb->addOrderBy($key, strtoupper($aSearchSort));
            }
        }
        else {
            $qb->orderBy('id', 'ASC');
        }

        if (isset($data['page']) && isset($data['perPage'])) {
            // now get one page's items:
            $qb->setFirstResult($data['perPage'] * ($data['page']-1)) // set the offset
            ->setMaxResults($data['perPage']); // set the limit

            $dataDB = $qb->execute()->fetchAll();

            $dataDB = $this->returnNodes($app, $dataDB, $data);

            $dataDB = $this->returnRelationshipArray($app, $dataDB, $this->metadata, $data);

            $aReturn['data'] = $dataDB;

            $qb = $this->db->createQueryBuilder();

            $qb->select('( COUNT(id) )  as count')
                ->from($this->metadata->table['name']);
            $countRows= $qb->execute()->fetch();

            $aReturn['pagination']['total'] = $countRows['count'];
            $aReturn['pagination']['pages'] = ceil($countRows['count']/$data['perPage']);
        }
        else {

            $dataDB = $qb->execute()->fetchAll();

            $dataDB = $this->returnNodes($app, $dataDB, $data);

            $dataDB = $this->returnRelationshipArray($app, $dataDB, $this->metadata, $data);

            if (count($dataDB) == 0) {
                throw new InvalidArgumentHttpException("Nenhum registro encontrado.");
            }

            $aReturn['data'] = $dataDB;
        }

        return $aReturn;
    }

    public function getAll(Application $app)
    {
        $request = $app['request'];

        $aReturn = ["code" => 200, "message" => "Mensagem Teste"];

        $data = TransformRequest::transformQueryToArray($request);

        $aReturn = $this->customQuery($data, $app);

        return new JsonResponse($aReturn);
    }

    public function getById(Application $app)
    {
        $request = $app['request'];

        $id = $request->attributes->get('id');

        $data = TransformRequest::transformQueryToArray($request);

        if (isset($data['search'])) {
            $data['search'] .= ',id='.$id;
        }
        else {
            $data['search'] = 'id='.$id;
        }

        $dataDB = $this->customQuery($data, $app);

        return new JsonResponse(["code" => 200, "message" => "Mensagem Teste", "data" => $dataDB['data'][0]]);
    }

    public function post(Application $app)
    {
        $request = $app['request'];

        $data = TransformRequest::transformContentToArray($request);

        $newData[substr(strrchr($this->metadata->name, "\\"), 1)] = $data;

        $aResponse = $this->parsePostPut($app, $newData);

        return new JsonResponse(["code" => 200, "message" => "POST", "data" => $aResponse]);
    }

    public function update(Application $app)
    {
        $request = $app['request'];

        $dataUpdate = TransformRequest::transformContentToArray($request);

        $aResponse = $this->parsePostPut($app, $dataUpdate);

//        $id = $dataUpdate['id'];
//
//        unset($dataUpdate['id']);
//
//        $aResponse = $this->persist($app, 'update', $dataUpdate, array('id' => $id));

        return new JsonResponse(["code" => 200, "message" => "PUT", "data" => $aResponse]);
    }

    public function delete(Application $app)
    {
        $request = $app['request'];

        $id = TransformRequest::getAttribute($request, 'id');

        $aResponse = $this->persist($app, 'delete', array(), array('id' => $id));

        return new JsonResponse(["code" => 200, "message" => "DELETE", "data" => $aResponse]);
    }

    public function findByFields($fields, $app)
    {
        $aReturn = array();

        $aDataDb = $this->customQuery(array("searchArray" => $fields), $app);

        return $aDataDb['data'];
    }

    public function findOneByField($fieldName, $value, $app)
    {
        $aReturn = array();

        $aDataDb = $this->customQuery(array("searchArray" => [$fieldName => $value]), $app);

        return $aDataDb['data'][0];
    }

    public function parsePostPut(Application $app, $data, $condition = array()) {

        $aIDs = array();

        //Fazendo inserts many to one
        foreach ($data as $key => $item) {
            //Se é um relacionamen one to one

            if (is_array($item) && !is_array($item[array_keys($item)[0]])) {
                foreach ($item as $keyTwo => $itemTwo) {
                    if (is_array($itemTwo) && !is_array($itemTwo[array_keys($itemTwo)[0]])) {
                        foreach ($itemTwo as $keyThree => $itemThree) {
                            if (is_array($itemThree) && !is_array($itemThree[array_keys($itemThree)[0]])) {
//                                echo "INSERT INTO ".ucfirst($keyThree)." VALUES (3333AA)\n";

                                $entityManager = $app['Doctrine\ORM\EntityManager'];
                                $repo = $entityManager->getRepository('API\Entity\\'.ucfirst($keyThree));

                                $abstractController = new AbstractController($app, $repo);

                                $action = empty($itemThree['id']) ? 'insert' : 'update';
                                $condition = array();

                                if ($action == 'update') {
                                    $condition = array('id' => $itemThree['id']);
                                }

                                $return = $abstractController->persist($app, $action, $itemThree, $condition);

                                $data[$key][$keyTwo][strtolower('id_'.$keyThree)] = $return['id'];
                                $aIDs[strtolower('id_'.$keyThree)] = $return['id'];

                                unset($data[$key][$keyTwo][$keyThree]);
                            }
                        }

                        $entityManager = $app['Doctrine\ORM\EntityManager'];
                        $repo = $entityManager->getRepository('API\Entity\\'.ucfirst($keyTwo));

                        $abstractController = new AbstractController($app, $repo);
                        $action = empty($itemTwo['id']) ? 'insert' : 'update';
                        $condition = array();

                        if ($action == 'update') {
                            $condition = array('id' => $itemTwo['id']);
                        }

                        $return = $abstractController->persist($app, $action, $itemTwo, $condition);

                        $data[$key][strtolower('id_'.$keyTwo)] = $return['id'];
                        unset($data[$key][$keyTwo]);

                        $aIDs[strtolower('id_'.$keyTwo)] = $return['id'];

//                        echo "INSERT INTO ".ucfirst($keyTwo)." VALUES (2222A)\n";
                    }
                }

                foreach ($item as $keyTwoArray => $itemTwoArray) {
                    if (is_array($itemTwoArray)) {
                        foreach ($itemTwoArray as $keyThreeArray => $itemThreeArray) {
                            if (is_array($itemThreeArray) && is_array($itemThreeArray[array_keys($itemThreeArray)[0]])) {
                                foreach ($itemThreeArray as $keyFourArray => $itemFourArray) {
//                                    echo "INSERT INTO " . ucfirst($keyThreeArray) . " VALUES (3333A".$keyFourArray.")\n";

                                    $itemFourArray[strtolower('id_'.$keyTwoArray)] = $aIDs[strtolower('id_'.$keyTwoArray)];

                                    $entityManager = $app['Doctrine\ORM\EntityManager'];
                                    $repo = $entityManager->getRepository('API\Entity\\'.ucfirst($keyThreeArray));

                                    $abstractController = new AbstractController($app, $repo);

                                    $action = empty($itemFourArray['id']) ? 'insert' : 'update';
                                    $condition = array();

                                    if ($action == 'update') {
                                        $condition = array('id' => $itemFourArray['id']);
                                    }

                                    $return = $abstractController->persist($app, $action, $itemFourArray, $condition);
                                }
                                unset($data[$key][$keyTwoArray]);
                            }
                        }
                    }
                }
            }
        }

        foreach ($data as $keyFirst => $itemFirst) {
            $entityManager = $app['Doctrine\ORM\EntityManager'];
            $repo = $entityManager->getRepository('API\Entity\\'.ucfirst($keyFirst));

            $abstractController = new AbstractController($app, $repo);

            $action = empty($itemFirst['id']) ? 'insert' : 'update';
            $condition = array();

            if ($action == 'update') {
                $condition = array('id' => $itemFirst['id']);
            }

            $return = $abstractController->persist($app, $action, $itemFirst, $condition);

            $aIDs[strtolower('id')] = $return['id'];
            $aIDs[strtolower('id_'.$keyFirst)] = $return['id'];
            $data[strtolower('id_'.$keyFirst)] = $return['id'];

//            unset($data[$keyFirst]);

//            echo "INSERT INTO ".ucfirst($keyFirst)." VALUES (1111)\n";
        }

        foreach ($data as $keyManyManyTwo => $itemManyManyTwo) {
            if (is_array($itemManyManyTwo) && !is_array($itemManyManyTwo[array_keys($itemManyManyTwo)[0]])) {

                foreach ($itemManyManyTwo as $keyTwoManyArray => $itemTwoManyArray) {
                    if (is_array($itemTwoManyArray)) {
                        foreach ($itemTwoManyArray as $keyThreeManyArray => $itemThreeManyArray) {

                            if (is_array($itemThreeManyArray) && !is_array($itemThreeManyArray[array_keys($itemThreeManyArray)[0]])) {
//                                echo "INSERT INTO " . ucfirst($keyTwoManyArray) . " VALUES (3333B".$keyThreeManyArray.")\n";

                                $itemThreeManyArray[strtolower('id_'.$keyManyManyTwo)] = $aIDs[strtolower('id_'.$keyManyManyTwo)];

                                $entityManager = $app['Doctrine\ORM\EntityManager'];
                                $repo = $entityManager->getRepository('API\Entity\\'.ucfirst($keyTwoManyArray));

                                $abstractController = new AbstractController($app, $repo);

                                $action = empty($itemThreeManyArray['id']) ? 'insert' : 'update';
                                $condition = array();

                                if ($action == 'update') {
                                    $condition = array('id' => $itemThreeManyArray['id']);
                                }

                                $abstractController->persist($app, $action, $itemThreeManyArray, $condition);
                            }
                        }

                        unset($data[$keyManyManyTwo][$keyTwoManyArray]);
                    }
                }
            }
        }

        return $aIDs;
    }

    /**
     * @param Application $app
     * @param $method
     * @param $data
     * @param array $condition
     * @return mixed
     * @throws Exception
     * @throws \Exception
     */

    public function persist (Application $app, $method, $data, $condition = array()) {
        $this->db->beginTransaction();
        try {
            $now = new \DateTime("now");

            if ($method == 'insert') {
                $data['id'] = mt_rand(10000, 99999 ).time();

                $data['created_at'] = $now->format('Y-m-d H:i:s');
            }
            else if ($method == 'update') {
                $data['updated_at'] = $now->format('Y-m-d H:i:s');
            }
            else if ($method == 'delete') {
                $method = 'update';
                $data['deleted_at'] = $now->format('Y-m-d H:i:s');
            }

            //Limpa campos que são arrays
            foreach ($data as $key => $item) {
                if (is_array($data[$key])) {
                    unset($data[$key]);
                }
            }

            $return = $this->db->$method($this->metadata->table['name'], $data, $condition);

            $this->db->commit();

            $aResponse = [];

            if ($method == 'insert' || $method == 'update') {
                $aResponse['id'] = $data['id'];
            }

            return $aResponse;
        } catch(Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    public function returnRelationshipArray (Application $app, $dataDB, $metadata, $requestData=array()) {

        $joins = $metadata->associationMappings;

        if (count($joins) > 0) {
            foreach($dataDB as $key => $dataItem){
                if (count($joins) > 0) {
                    foreach ($joins as $join) {

                        $fieldName = str_replace('API\\Entity\\', '', $join['targetEntity']);

                        if ($join['mappedBy']) {
                            $entityManager = $app['Doctrine\ORM\EntityManager'];
                            $repo = $entityManager->getRepository($join['targetEntity']);
                            $metadata = $this->em->getClassMetadata($join['targetEntity']);

                            if (empty($requestData['objects']) || (!empty($requestData['objects']) && in_array($fieldName, $requestData['objects']) === false)) {
                                continue;
                            }

                            /** @var QueryBuilder $qbTemp */
                            $qbTemp = $this->db->createQueryBuilder();

                            $qbTemp->select('*')
                                ->from($metadata->table['name'])
                                ->where('deleted_at IS NULL')
                                ->where('id_' . $join['mappedBy'] . ' = :id')
                                ->setParameter(':id', $dataItem['id'])
                                ->orderBy('id', 'ASC');

                            $aQueryDB = $qbTemp->execute()->fetchAll();

                            if (count($aQueryDB) > 0) {
                                $dataDB[$key][$fieldName] = $aQueryDB;
                            }
                        }
                    }
                }
            }
        }

        return $dataDB;
    }

    public function returnNodes (Application $app, $dataDB, $requestData) {

        $joins = $this->metadata->associationMappings;

        if (count($joins) > 0) {
            foreach($dataDB as $key => $dataItem){
                foreach($joins as $join) {

                    $fieldName = str_replace('API\\Entity\\', '', $join['targetEntity']);

                    if (isset($join['joinColumns']) && (isset($dataItem[$join['joinColumns'][0]['name']]))) {
                        $metadata = $this->em->getClassMetadata($join['targetEntity']);

                        if (empty($requestData['objects']) || (!empty($requestData['objects']) && in_array($fieldName, $requestData['objects']) === false)) {
                            continue;
                        }

                        /** @var QueryBuilder $qbTemp */
                        $qbTemp = $this->db->createQueryBuilder();

                        $qbTemp->select('*')
                            ->from($metadata->table['name'])
                            ->where('deleted_at IS NULL')
                            ->where('id = :id')
                            ->setParameter(':id', $dataItem[$join['joinColumns'][0]['name']])
                            ->orderBy('id', 'ASC');

                        $aQueryDB = $qbTemp->execute()->fetchAll();

                        if (count($aQueryDB) > 0) {

                            $dataDB[$key][$fieldName] = count($aQueryDB) == 1 ? $aQueryDB[0] : $aQueryDB;

                            $entityManager = $app['Doctrine\ORM\EntityManager'];
                            $repo = $entityManager->getRepository($join['targetEntity']);
                            $metadata = $this->em->getClassMetadata($join['targetEntity']);

                            $dataDB[$key][$fieldName] = $this->returnRelationshipArray($app, array($dataDB[$key][$fieldName]), $metadata, $requestData);
                        }
                    }
                }
            }
        }

        return $dataDB;
    }
}

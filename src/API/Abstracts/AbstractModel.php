<?php

namespace API\Abstracts;

use API\Entity\EntityInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractRetrieveService
 * @package API\Services\Abstracts
 */
abstract class AbstractRetrieveService
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @param int $id
     * @return \API\Entity\EntityInterface
     */
    public function retrieve($id)
    {
        return $this->entityRepository->findOneBy(['id' =>  $id]);
    }

    /**
     * @return array
     */
    public function retrieveAll()
    {
        return $this->entityRepository->findAll();
    }

    /**
     * @param string $fieldName
     * @param mixed $value
     * @return EntityInterface
     */
    public function retrieveByField($fieldName, $value)
    {
        return $this->entityRepository->findOneBy([$fieldName => $value]);
    }
}

<?php
namespace API\Abstracts;

use API\Abstracts\AbstractService;
use Silex\Application;

class AbstractController extends AbstractService
{
    function __construct($app, $em)
    {
        parent::__construct($app, $em);
    }
}

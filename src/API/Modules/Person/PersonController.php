<?php
namespace API\Modules\Person;

use API\Abstracts\AbstractService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class PersonController extends AbstractService
{
    function __construct($app, $em)
    {
        parent::__construct($app, $em);
    }
}

<?php
use Silex\Application;

$app['api.auth'] = (function ($app) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository('API\Entity\Auth');

    return new API\Modules\Auth\AuthController($app, $repo);
});

$app->post('/autenticate', 'api.auth:autenticate');

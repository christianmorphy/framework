<?php
use Silex\Application;

$app['api.user'] = (function ($app) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository('API\Entity\User');

    return new API\Modules\User\UserController($app, $repo);
});

$app->post('/autenticate', 'api.user:autenticate');

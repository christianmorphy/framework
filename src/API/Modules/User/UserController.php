<?php
namespace API\Modules\User;

use API\Abstracts\AbstractService;
use Common\Util\TransformRequest;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends AbstractService
{
    function __construct($app, $repo)
    {
        parent::__construct($app, $repo);
    }

    public function Autenticate(Application $app) {
        $request = $app['request'];
        $data = TransformRequest::transformContentToArray($request);

        $dataDB = $this->findByFields($data, $app);

        $aReturn = $dataDB[0];
        $aReturn['token'] = hash('sha256', uniqid(rand(), true));

        $result = $this->persist($app, 'update', array('token' => $aReturn['token']), array('id' => $aReturn['id']));

        return new JsonResponse(["code" => 200, "message" => "Mensagem Teste", "data" => $aReturn]);
    }
}

<?php
namespace API\Modules\Contact;

use API\Abstracts\AbstractService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContactController extends AbstractService
{
    function __construct($app, $em)
    {
        parent::__construct($app, $em);
    }

    public function getTeste(Application $app) {
        $sql = 'SELECT * FROM tb_contact WHERE id = 9';
        $post = $app['db']->fetchAssoc($sql);

        return new JsonResponse(["code" => 200, "message" => "Mensagem Teste", "data" => $post]);
    }
}

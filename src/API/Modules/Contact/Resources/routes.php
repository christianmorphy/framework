<?php
use Silex\Application;

$app['api.contact'] = (function ($app) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $app['Doctrine\ORM\EntityManager'];
    $repo = $entityManager->getRepository('API\Entity\Contact');

    return new API\Modules\Contact\ContactController($app, $repo);
});

//$app->get('/contact/{id}', 'api.contact:getById');
//$app->get('/contact', 'api.contact:getAll');
//$app->post('/contact', 'api.contact:post');
//$app->put('/contact', 'api.contact:update');
//$app->delete('/contact/{id}', 'api.contact:delete');
<?php
namespace API\Tools\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Doctrine\ORM\Tools\Console\MetadataFilter;
use Doctrine\ORM\Tools\EntityRepositoryGenerator;
use Doctrine\ORM\Tools\EntityGenerator;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Command to generate entity classes and method stubs from your mapping information.
 *
 * @link    www.doctrine-project.org
 * @since   2.0
 * @author  Benjamin Eberlei <kontakt@beberlei.de>
 * @author  Guilherme Blanco <guilhermeblanco@hotmail.com>
 * @author  Jonathan Wage <jonwage@gmail.com>
 * @author  Roman Borschel <roman@code-factory.org>
 */
class GenerateStructureCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('orm:generate-structure')
            ->setAliases(array('orm:generate:structure'))
            ->setDescription('Generate entity and repositories classes from your mapping information.')
            ->setDefinition(array(
                new InputArgument(
                    'dest-path', InputArgument::REQUIRED, 'The path to generate your entity classes.'
                ),
                new InputOption(
                    'regenerate-entities', null, InputOption::VALUE_OPTIONAL,
                    'Flag to define if generator should regenerate entity if it exists.', false
                ),
                new InputOption(
                    'update-entities', null, InputOption::VALUE_OPTIONAL,
                    'Flag to define if generator should only update entity if it exists.', true
                )
            ))
            ->setHelp(<<<EOT
Generate entity and repositories classes from your mapping information.

If you use the <comment>--update-entities</comment> or <comment>--regenerate-entities</comment> flags your existing
code gets overwritten. The EntityGenerator will only append new code to your
file and will not delete the old code. However this approach may still be prone
to error and we suggest you use code repositories such as GIT or SVN to make
backups of your code.
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getHelper('em')->getEntityManager();

        $cmf = new DisconnectedClassMetadataFactory();
        $cmf->setEntityManager($em);
        $metadatas = $cmf->getAllMetadata();

        // Process destination directory
        $destPath = realpath($input->getArgument('dest-path'));

        if ( ! file_exists($destPath)) {
            throw new \InvalidArgumentException(
                sprintf("Entities destination directory '<info>%s</info>' does not exist.", $input->getArgument('dest-path'))
            );
        }

        if ( ! is_writable($destPath)) {
            throw new \InvalidArgumentException(
                sprintf("Entities destination directory '<info>%s</info>' does not have write permissions.", $destPath)
            );
        }

        if (count($metadatas)) {
            // Create EntityGenerator
            $entityGenerator = new EntityGenerator();

            $entityGenerator->setRegenerateEntityIfExists($input->getOption('regenerate-entities'));
            $entityGenerator->setUpdateEntityIfExists($input->getOption('update-entities'));

            foreach ($metadatas as $metadata) {
                $output->writeln(
                    sprintf('Processing entity "<info>%s</info>"', $metadata->name)
                );
            }

            // Generating Entities
            $entityGenerator->generate($metadatas, $destPath);

            // Outputting information message
            $output->writeln(PHP_EOL . sprintf('Entity classes generated to "<info>%s</INFO>"', $destPath));

            // Generate repositories
            $repositoryName = $em->getConfiguration()->getDefaultRepositoryClassName();

            if ( ! file_exists($destPath)) {
                throw new \InvalidArgumentException(
                    sprintf("Entities destination directory '<info>%s</info>' does not exist.", $input->getArgument('dest-path'))
                );
            }

            if ( ! is_writable($destPath)) {
                throw new \InvalidArgumentException(
                    sprintf("Entities destination directory '<info>%s</info>' does not have write permissions.", $destPath)
                );
            }

            if (count($metadatas)) {
                $numRepositories = 0;
                $generator = new EntityRepositoryGenerator();

                $generator->setDefaultRepositoryName($repositoryName);

                foreach ($metadatas as $metadata) {
                    if ($metadata->customRepositoryClassName) {
                        $output->writeln(
                            sprintf('Processing repository "<info>%s</info>"', $metadata->customRepositoryClassName)
                        );

                        $generator->writeEntityRepositoryClass($metadata->customRepositoryClassName, $destPath);

                        $numRepositories++;
                    }
                }

                if ($numRepositories) {
                    // Outputting information message
                    $output->writeln(PHP_EOL . sprintf('Repository classes generated to "<info>%s</INFO>"', $destPath) );
                } else {
                    $output->writeln('No Repository classes were found to be processed.' );
                }
            } else {
                $output->writeln('No Metadata Classes to process.' );
            }
        } else {
            $output->writeln('No Metadata Classes to process.');
        }
    }
}

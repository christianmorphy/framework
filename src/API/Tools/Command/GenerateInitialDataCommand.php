<?php
namespace API\Tools\Command;

use API\Entity\Repository\UserRepository;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManager;

class GenerateInitialDataCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('orm:generate-initial-data')
            ->setDescription('Generate initial data (Users).')
            ->setDefinition(array())
            ->setHelp(<<<EOT
Generate initial data. (Users)
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getHelper('em')->getEntityManager();

        /** @var UserRepository $repo */
        $repo = $em->getRepository('API\\Entity\\User');
        $conn = $em->getConnection();

        $queryBuilder = $em->createQueryBuilder();

        $cmf = new DisconnectedClassMetadataFactory();
        $cmf->setEntityManager($em);
        $metadatas = $cmf->getAllMetadata();

        $em->beginTransaction();
        try{

            $aDataRole = array();

            $aDataRole['id'] = (int) mt_rand(10000, 99999).time();
            $aDataRole['reference_code'] = md5(uniqid(rand(), true));
            $aDataRole['name'] = 'Administrator';
            $aDataRole['slug'] = 'ADMIN';
            $aDataRole['description'] = 'Administrator';

            $conn->insert('tb_role', $aDataRole);

            $idRole = $aDataRole['id'];

            $aDataPerson = array();

            $aDataPerson['id'] = (int) mt_rand(10000, 99999 ).time();
            $aDataPerson['reference_code'] = md5(uniqid(rand(), true));
            $aDataPerson['type'] = 'FÍSICA';
            $aDataPerson['name'] = 'Christian James';
            $aDataPerson['email'] = 'contato@christianjames.com.br';

            $conn->insert('tb_person', $aDataPerson);

            $idPerson = $aDataPerson['id'];

            $aDataUser = array();
            $aDataUser['id'] = (int) mt_rand(10000, 99999 ).time();
            $aDataUser['reference_code'] = md5(uniqid(rand(), true));
            $aDataUser['id_person'] = $idPerson;
            $aDataUser['id_role'] = $idRole;
            $aDataUser['email'] = 'contato@christianjames.com.br';
            $aDataUser['password'] = 'password';

            $conn->insert('tb_user', $aDataUser);
            $em->commit();

            $aResponse = [];
            $aResponse['id'] = $aDataUser['id'];

            $output->writeln(
                sprintf('Usuário criado "<info>%s</info>"', $aResponse['id'])
            );
        } catch(Exception $e) {
            $em->rollBack();
            throw $e;
        }
    }
}

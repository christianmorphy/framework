<?php

namespace API\Entity;

/**
 * City
 */
class City
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $id_state;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $uf;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $personAddresses;

    /**
     * @var \API\Entity\State
     */
    private $state;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personAddresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

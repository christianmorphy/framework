<?php

namespace API\Entity;

/**
 * UserQuotum
 */
class UserQuotum
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_user;

    /**
     * @var float
     */
    private $value;

    /**
     * @var \API\Entity\User
     */
    private $user;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

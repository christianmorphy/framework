<?php

namespace API\Entity;

/**
 * PersonAddress
 */
class PersonAddress
{
    /**
     * @var integer
     */
    private $id_address;

    /**
     * @var integer
     */
    private $id_person;

    /**
     * @var integer
     */
    private $id_city;

    /**
     * @var string
     */
    private $type;

    /**
     * @var boolean
     */
    private $is_default;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $zip_code;

    /**
     * @var \API\Entity\Person
     */
    private $person;

    /**
     * @var \API\Entity\City
     */
    private $city;


    /**
     * @var integer
     */
    private $id;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \DateTime
     */
    private $updated_at;


}

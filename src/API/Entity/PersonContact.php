<?php

namespace API\Entity;

/**
 * PersonContact
 */
class PersonContact
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_person;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $type;

    /**
     * @var boolean
     */
    private $is_default;

    /**
     * @var \API\Entity\Person
     */
    private $person;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

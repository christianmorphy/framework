<?php

namespace API\Entity;

/**
 * Price
 */
class Price
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_table_price;

    /**
     * @var string
     */
    private $sku_product;

    /**
     * @var float
     */
    private $value;

    /**
     * @var \API\Entity\TablePrice
     */
    private $tablePrice;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

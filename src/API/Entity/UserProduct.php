<?php

namespace API\Entity;

/**
 * UserProduct
 */
class UserProduct
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_user;

    /**
     * @var string
     */
    private $sku_product;

    /**
     * @var \API\Entity\User
     */
    private $user;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

<?php

namespace API\Entity;

/**
 * OrderItem
 */
class OrderItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_order;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $sku_product;

    /**
     * @var string
     */
    private $name_product;

    /**
     * @var float
     */
    private $price_procuct;

    /**
     * @var string
     */
    private $customization;

    /**
     * @var \API\Entity\Order
     */
    private $order;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

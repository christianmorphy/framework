<?php

namespace API\Entity;

/**
 * UserCart
 */
class UserCart
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_user;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $sku_product;

    /**
     * @var string
     */
    private $name_product;

    /**
     * @var float
     */
    private $price_procuct;

    /**
     * @var string
     */
    private $customization;

    /**
     * @var \API\Entity\User
     */
    private $user;


    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

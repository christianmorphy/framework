<?php

namespace API\Entity;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_person;

    /**
     * @var integer
     */
    private $id_role;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $token;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orders;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tablePrices;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userCarts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPermissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userProducts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userQuota;

    /**
     * @var \API\Entity\Person
     */
    private $person;

    /**
     * @var \API\Entity\Role
     */
    private $role;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $PermissionGroups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tablePrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userCarts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userPermissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userQuota = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PermissionGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \DateTime
     */
    private $updated_at;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPermissionGroups;


}

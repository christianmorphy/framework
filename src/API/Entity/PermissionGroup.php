<?php

namespace API\Entity;

/**
 * PermissionGroup
 */
class PermissionGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Permissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Permissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $permissionGroupPermissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPermissionGroups;


}

<?php

namespace API\Entity;

/**
 * UserPermissionGroup
 */
class UserPermissionGroup
{
    /**
     * @var integer
     */
    private $id_user;

    /**
     * @var integer
     */
    private $id_permission_group;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \API\Entity\User
     */
    private $user;

    /**
     * @var \API\Entity\PermissionGroup
     */
    private $permissionGroup;


}

<?php

namespace API\Entity;

/**
 * UserPermission
 */
class UserPermission
{
    /**
     * @var integer
     */
    private $id_permission;

    /**
     * @var integer
     */
    private $id_user;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \API\Entity\Permission
     */
    private $permission;

    /**
     * @var \API\Entity\User
     */
    private $user;


    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;


}

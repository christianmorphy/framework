<?php

namespace API\Entity;

/**
 * Permission
 */
class Permission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $id_menu;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPermissions;

    /**
     * @var \API\Entity\Menu
     */
    private $menu;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $PermissionGroups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userPermissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PermissionGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $permissionGroupPermissions;


}

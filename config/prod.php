<?php
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;

$applicationMode = "development";

$config = new Configuration();

if (getenv('ENVIRONMENT') != 'PRODUCTION') {
    $cache = new \Doctrine\Common\Cache\ArrayCache;
    //TODO Descomentar ao enviar para producao
    $cache->flushAll();
} else {
    $cache = new \Doctrine\Common\Cache\ApcuCache;
}

$config = new Configuration;
$config->setMetadataCacheImpl($cache);

$driver = new SimplifiedYamlDriver([
    __DIR__ . '/../src/Mapping' => 'API\Entity',
]);

$config->setMetadataDriverImpl($driver);
$config->setQueryCacheImpl($cache);
$config->setProxyDir('../data/Proxies');
$config->setProxyNamespace('EntityProxy');

if ($applicationMode == "development") {
    $config->setAutoGenerateProxyClasses(true);
} else {
    $config->setAutoGenerateProxyClasses(false);
}

$connectionOptions = array(
    'driver' => 'pdo_mysql',
    'host' => getenv('HOST'),
    'port' => getenv('PORT'),
    'user' => getenv('USER'),
    'password' => getenv('PASSWORD'),
    'dbname' => getenv('DATABASE')
);

$entityManager = EntityManager::create($connectionOptions, $config);

$app['Doctrine\ORM\EntityManager'] = $entityManager;

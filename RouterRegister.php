<?php
//$Routers = array();
$Routers = array(
    'index' => array('app'=>'Site',
				     'controller' => 'index',
					 'action' => 'index',
					 'lang' => 'en',
					),
    'Admin/login' => array('app'=>'Admin',
				     'controller' => 'autentica',
					 'action' => 'index',
					 'lang' => 'pt-br',
					 'isAuth' => false,
					),
    'Admin/teste' => array('app'=>'Admin',
				     'controller' => 'autentica',
					 'action' => 'index',
					 'lang' => 'pt-br',
					 'isAuth' => false,
					 'redirect' => '/Admin/teste2',
					),
    'Admin/logoff' => array('app'=>'Admin',
				     'controller' => 'autentica',
					 'action' => 'logoff',
					 'lang' => 'pt-br',
					),
    'logoff' => array('app'=>'Site',
				     'controller' => 'index',
					 'action' => 'logoff',
					 'lang' => 'pt-br'
					),
    'Contato' => array('app'=>'Site',
				     'controller' => 'Contato',
					 'action' => 'index',
					 'lang' => 'pt-br'
					),
    'Contato/:teste' => array('app'=>'Site',
				     'controller' => 'Contato',
					 'action' => 'index',
					 'lang' => 'pt-br'
					),
);
?>

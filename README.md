# Framework Silex / Twig #

## Repo Cidades, Estados, Regi�es
* https://github.com/chandez/Estados-Cidades-IBGE

## Configuration ##

>composer install

**APP_DEFAULT** (Name Bundle)
For configure application without app name in URL.

**Apache**

SetEnv APP_DEFAULT "API"

**Nginx**

fastcgi_param   DEFAULT  API;
## Commands ##

### Generate Structure ###
**Entities YML and Repositories**

>php bin/doctrine orm:generate-structure src/ --update-entities

OR

>php vendor/doctrine/dbal/bin/doctrine-dbal orm:generate-structure src/ --update-entities

**Generate Initial Data (Users)**
>php bin/doctrine orm:generate-initial-data

OR

>php vendor/doctrine/dbal/bin/doctrine-dbal orm:generate-initial-data

## Default Routes ##

>/application/controller/action/id

>/application/controller/getAll

>/application/controller/getByReferenceCode/:reference_code

### Filters ###

>/application/controller/getAll?fields=name,email

>/application/controller/getAll?fields=name,email&sort=name:asc,email:desc

### Search ###

> operators: like (like), = (equal), <> (diff), > (moreThan), >= (moreEqualThan), < (less), <= (lessThan)

>>Example: /API/User/getAll?search=expr=or|op=equal|name=Christian|email=contato@christianjames
>>Example: /API/User/getAll?search=op=like|email=%christianjames2%

### Relationship
 Tem que colocar nos fields do id do relacionamento e no objects o nome da Entidade.
 Por exemplo: Eu quero que retorna Person, ent�o eu coloca o id_person no fields e o Person no objects, ficaria assim: ?fields=id,id_person&objects=Person
> http://host/api/index.php/API/User/getAll?fields=id,id_person,email,password&objects=Person,PersonAddress

### Generate Documentation ###
>./bin/swagger src/ -o web/swagger/swagger.json

